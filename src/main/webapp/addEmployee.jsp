
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <h1>Add employee</h1>
        <form  action="/AimprosoftTestTwo/EmployeeList" method="post">
            <table>
                <tr>${invalid}</tr>
                <tr>
                    <td>FirstName</td>
                    <td>SecondName</td>
                    <td>Email</td>
                    <td>Salary</td>
                    <td>DateofBirth</td>
                </tr>
                <tr>
                    <td><input type="text" name = "firstName" value="${firstName}" ></td>
                    <td><input type="text" name = "secondName" value="${secondName}"></td>
                    <td><input type="text" name="email" value="${email}"></td>
                    <td><input type="text" name="salary"></td>
                    <td><input type="date" name="date"></td>
                   
                </tr>
                <tr>
                    <td>
                        <input type = "submit" name="addEmployee" value="Add">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
