

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update employee information</title>
    </head>
    <body>
        <h1>Update Employee</h1>
        <table>
            <form action="/AimprosoftTestTwo/UpdateEmployee" method="post">
                <tr>${invalid}</tr>
                <tr> <th>Update Employee ${employeeUpdate.firstname}</th></tr>
                <tr>
                    <td>FirstName</td>
                    <td>SecondName</td>
                    <td>Email</td>
                    <td>Salary</td>
                    <td>DateofBirth</td>
                </tr>
                <tr>
                    <td><input type="text" name = "firstName" value=${employeeUpdate.firstname}></td>
                    <td><input type="text" name = "secondName" value=${employeeUpdate.secondname}></td>
                    <td><input type="text" name="email" value=${employeeUpdate.email}></td>
                    <td><input type="text" name="salary" value=${employeeUpdate.salary}></td>
                    <td><input type="date" name="dateOfBirth" value=${employeeUpdate.dateOfBirth}></td>
                </tr>
                <tr>
                    <td><input type="submit" value="save"></td>
                </tr>
            </form>
        </table>


    </body>
</html>
