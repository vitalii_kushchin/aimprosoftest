
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of departmants</title>
    </head>
    <body>
        <table>
            <tr>
            <th>Name of Departmant</th>
            <th>Departmant email</th>
            </tr>
            <c:forEach var="departmant" items="${departmantList}">
                <tr>
                <td>${departmant.name}</td>
                <td>${departmant.email}</td>
                <td><a href=EmployeeList?idDepartmant=${departmant.id}><input type="submit" value="employees"></a></td>
                <td><a href=UpdateDepartmant?idDepartmant=${departmant.id}><input type="submit" value="update"></a></td>
                <td><a href=DeleteDepartmant?idDepartmant=${departmant.id}> <input type="submit" value="delete"></a></td>        
                </tr>
            </c:forEach>
                <tr>
                    <td><a href="addDepartmant.jsp"><input type="submit" value="Add new"></a></td>
                </tr>
        </table>
    </body>
</html>
