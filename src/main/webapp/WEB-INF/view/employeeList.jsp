<%-- 
    Document   : employeeList
    Created on : 19.07.2019, 0:17:25
    Author     : Family
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employees in Departmant</title>
    </head>
    <body>
        <h1>Employees List</h1>
        <table>
            <tr>
                <th>Firstname</th>
                <th>Secondname</th>
                <th>Email</th>
                <th>Salary</th>
                <th>DateOfBirth</th>
            </tr>
            <c:forEach var="employee" items="${employeesList}">
                <tr>
                <td>${employee.firstname}</td>
                <td>${employee.secondname}</td>
                <td>${employee.email}</td>
                <td>${employee.salary}</td>
                <td>${employee.dateOfBirth}</td> 
                <td><a href=UpdateEmployee?employeeID=${employee.id}><input type="submit" value="update" name="update"></a></td>
                <td><a href=DeleteEmployee?employeeID=${employee.id}&idDepartmant=${employee.departmantID}><input type="submit" value="delete" name="delete"></a></td>
                </tr>
            </c:forEach>
                <td><a href ="addEmployee.jsp"><input type="submit" value="Add employee"></a></td>
            </table>
        </body>
    </html>
