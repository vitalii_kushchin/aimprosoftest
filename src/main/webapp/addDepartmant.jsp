
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Departmant</title>
    </head>
    <body>
        <form action="/AimprosoftTestTwo/DepartmantList" method="post">
            <table>
                <tr>${invalid}</tr>
                <tr>
                    <td>Departmant name:</td>
                    <td><input type="text" name="departmantName" value=${departmantName}></td>
                </tr>
                <tr>
                    <td>Departmant email:</td>
                    <td><input type="text" name="departmantEmail" value=${departmantEmail}></td> 
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Add departmant"> 
                    </td> 
                </tr>
            </table>
        </form>
    </body>
</html>
