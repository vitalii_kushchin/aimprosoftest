
package com.vitalii.kushchin.domain.mysql;

import com.vitalii.kushchin.domain.dao.*;
import com.vitalii.kushchin.domain.dao.entity.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoMySQL extends AbstractDao<Employee, Integer, String> implements EmployeeDao {

    public EmployeeDaoMySQL(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO employees (dateOfbirth, salary, firstname, secondname, email, idDepartmant) values (?,?,?,?,?,?) ";
    }

    @Override
    public String getReadQuery() {
        return "SELECT * FROM employees WHERE idemployees=?";
    }

    @Override
    public String getUpdateQuery() {
       
        return "UPDATE employees SET dateOfbirth=?, salary=?, firstname=?, secondname=?, email=?, idDepartmant=? WHERE idemployees = ?";
    }

    @Override
    public String getRemoveQuery() {
        return "DELETE FROM employees WHERE idemployees=?";
    }

    @Override
    public String selectAllQuery() {
        return "SELECT * FROM employees";
    }

    public String selectAllbyDepartmant() {
        return "SELECT * FROM employees where idDepartmant=?";
    }
    @Override
     public String getUnique() {
        return "SELECT * FROM employees WHERE EXISTS (SELECT * FROM employees WHERE email = ?)";
    }
     
    @Override
    public void parseStatementForCreate(PreparedStatement statement, Employee object) {
        try {
           
            statement.setDate(1, object.getDateOfBirth());
            statement.setFloat(2, object.getSalary());
            statement.setString(3, object.getFirstname());
            statement.setString(4, object.getSecondname());
            statement.setString(5, object.getEmail());
            statement.setInt(6, object.getDepartmantID());
        } catch (Exception ex) {
            System.out.println("Some problem EmployeeDaoImpl.parseStatementForCreate()");
        }
    }

    @Override
    public void parseStatementForRead(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (Exception ex) {
            System.out.println("Some problem EmployeeDaoImpl.parseStatementForRead()");
        }
    }

    @Override
    public void parseStatementForRemove(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (Exception ex) {
            System.out.println("Some problem EmployeeDaoImpl.parseStatementForRemove()");
        }
    }

    @Override
    public void parseStatementForUpdate(PreparedStatement statement, Employee object) {
        try {
            statement.setDate(1, object.getDateOfBirth());
            statement.setFloat(2, object.getSalary());
            statement.setString(3, object.getFirstname());
            statement.setString(4, object.getSecondname());
            statement.setString(5, object.getEmail());
            statement.setInt(6, object.getDepartmantID());
            statement.setInt(7, object.getId());

        } catch (Exception ex) {
            System.out.println("Some problem EmployeeDaoImpl.parseStatementForUpdate()");
        }
    }

    @Override
    public List<Employee> getEmployeebyDepartmant(Integer idDepartmant) {
        List<Employee> list = null;
        try (PreparedStatement ps = connection.prepareStatement(selectAllbyDepartmant())) {
            ps.setInt(1, idDepartmant);
            list = parseResultSet(ps.executeQuery());
        } catch (Exception ex) {
            System.out.println("Some Problem EmployeeDaoImpl.getEmployeebyDepartmant()");
        }
        return list;
    }

    @Override
    public List<Employee> parseResultSet(ResultSet rs) {
        List<Employee> list = new ArrayList<>();
        Employee employee;
        try {
            while (rs.next()) {
                employee = new Employee();
                employee.setId(rs.getInt("idemployees"));
                employee.setDateOfBirth(rs.getDate("dateOfbirth"));
                employee.setSalary(rs.getFloat("salary"));
                employee.setFirstname(rs.getString("firstname"));
                employee.setSecondname(rs.getString("secondname"));
                employee.setEmail(rs.getString("email"));
                employee.setDepartmantID(rs.getInt("idDepartmant"));
                list.add(employee);

            }
        } catch (Exception ex) {
            System.out.println("Some Problem in EmployeeDaoImpl.parseResultSet()!!!!");

        }
        return list;
    }

}
