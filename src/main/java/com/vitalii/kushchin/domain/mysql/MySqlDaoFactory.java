package com.vitalii.kushchin.domain.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import com.vitalii.kushchin.domain.dao.*;

public class MySqlDaoFactory extends DaoFactory {

    private static final String URL = "jdbc:mysql://localhost:3306/jdbctest?useSSL=FALSE&serverTimezone=Europe/Kiev";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "19091994vitalii";
    Connection connection;

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Problem with download jdbc driver");
        }
    }

    public Connection getConnection() {

        try {
            this.connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            if (connection == null) {
                System.out.println("Exception in connection initialization");
            }
        } catch (SQLException ex) {
            System.out.println("Some problem with connection");
        }
        return this.connection;
    }

    @Override
    public DepartmantDao getDepartmantDao() {
        return new DepartmantDaoMySQL(this.getConnection());
    }

    @Override
    public EmployeeDao getEmployeeDao() {
        return new EmployeeDaoMySQL(this.getConnection());
    }

}
