
package com.vitalii.kushchin.domain.mysql;

import com.vitalii.kushchin.domain.dao.entity.Departmant;
import com.vitalii.kushchin.domain.dao.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DepartmantDaoMySQL extends AbstractDao<Departmant, Integer, String> implements DepartmantDao {

    public DepartmantDaoMySQL(Connection connection) {
        super(connection);
    }

    @Override
    public List<Departmant> parseResultSet(ResultSet rs) {
        List<Departmant> list = new ArrayList<>();
        Departmant departmant;
        try {
            while (rs.next()) {

                departmant = new Departmant();
                departmant.setId(rs.getInt("idDepartment"));
                departmant.setName(rs.getString("name"));
                departmant.setEmail(rs.getString("email"));
                list.add(departmant);

            }
        } catch (Exception ex) {
            System.out.println("Some problem in DepartmantDaoMySQL.parseResultSet()");
        }
        return list;

    }

    @Override
    public String selectAllQuery() {
        return "SELECT * FROM department";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO department (name, email) values (?,?)";
    }

    @Override
    public String getReadQuery() {
        return "SELECT * FROM department WHERE idDepartment=?";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE department SET name=?, email=?  where  idDepartment =?";
    }

    @Override
    public String getRemoveQuery() {
        return "DELETE FROM department WHERE idDepartment=?";
    }

    @Override
    public String getUnique() {
        return "SELECT * FROM department WHERE EXISTS (SELECT * FROM department WHERE name=?)";
    }

  
    
    

    @Override
    public void parseStatementForCreate(PreparedStatement statement, Departmant object) {
        try {
            statement.setString(1, object.getName());
            statement.setString(2, object.getEmail());
        } catch (Exception ex) {
            System.out.println("Some problem DepartmantDaoMySQL.parseStatementForCreate()");
        }
    }

    @Override
    public void parseStatementForRead(PreparedStatement statement, Integer id) {
        try {
           statement.setInt(1, id);
        } catch (Exception ex) {

        }
    }

    @Override
    public void parseStatementForUpdate(PreparedStatement statement, Departmant object) {
        try {
            statement.setString(1, object.getName());
            statement.setString(2, object.getEmail());
            statement.setInt(3, object.getId());
        } catch (Exception ex) {

        }
    }

    @Override
    public void parseStatementForRemove(PreparedStatement statement, Integer id) {
        try {
            statement.setInt(1, id);
        } catch (Exception ex) {

        }
    }

}
