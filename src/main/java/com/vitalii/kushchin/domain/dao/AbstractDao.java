package com.vitalii.kushchin.domain.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.util.List;

public abstract class AbstractDao<T, I extends Serializable, S extends String> implements BaseDao<T, I, S> {

    protected Connection connection;

    public AbstractDao(Connection connection) {
        this.connection = connection;
    }

    
    @Override
    public List<T> getAll()  {
        List<T> list = null;
        String selectQuery = selectAllQuery();
        try (PreparedStatement ps = connection.prepareStatement(selectQuery)) {
            list = parseResultSet(ps.executeQuery());

        } catch (Exception ex) {
            System.out.println("Some Problem with getAll!!!");
        }

        return list;
    }

    @Override
    public void creat(T object) {
        String creat = getCreateQuery();
        try (PreparedStatement ps = connection.prepareStatement(creat)) {
            parseStatementForCreate(ps, object);
            ps.execute();
        } catch (Exception ex) {
            System.out.println("Some Problem with creat!!!");
        }

    }

    @Override
    public T read(I id) {
        List<T> list = null;
        String read = getReadQuery();
        try (PreparedStatement ps = connection.prepareStatement(read)) {
            parseStatementForRead(ps, id);
            list = parseResultSet(ps.executeQuery());

        } catch (Exception ex) {
            System.out.println("Some Problem with read!!!");
        }
        if (list == null || list.size() == 0) {
            System.out.println("list == null or list.size==0");
        }
        if (list.size() > 1) {
            System.out.println("list.size > 0");
        }
        return list.iterator().next();
    }

    @Override
    public void update(T object) {
        String update = getUpdateQuery();
        try (PreparedStatement ps = connection.prepareStatement(update)) {
            parseStatementForUpdate(ps, object);
            ps.executeUpdate();
        } catch (Exception ex) {
               System.out.println("Some problem with update!!!!");
        }
    }

    @Override
    public void remove(I id) {
        String remove = getRemoveQuery();
        try (PreparedStatement ps = connection.prepareStatement(remove)) {
            parseStatementForRemove(ps, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println("Some problem with remove!!!!");
        }
    }

    @Override
    public boolean isUnique(S object) {
        boolean isUnique = true;
        String unique = getUnique();
        try(PreparedStatement ps = connection.prepareStatement(unique)){
            ps.setString(1, object);
           if (ps.executeQuery().first()){
           isUnique = false;
           }
        }catch(Exception ex){
            System.out.println(ex);
            System.out.println("Some problem with isUnique()");
        }
        System.out.println(isUnique);
        return isUnique;
    }
    
    

    public abstract String getRemoveQuery();

    public abstract String getUpdateQuery();

    public abstract String getReadQuery();

    public abstract String getCreateQuery();

    public abstract String selectAllQuery();
    
    public abstract String getUnique();
    
    public abstract void parseStatementForCreate(PreparedStatement statement, T object);
    
    public abstract void parseStatementForUpdate(PreparedStatement statement, T object);
    
    public abstract void parseStatementForRemove(PreparedStatement statement, I id);
    
    public abstract void parseStatementForRead(PreparedStatement statement, I id);
    
    public abstract List<T> parseResultSet(ResultSet rs);

}
