
package com.vitalii.kushchin.domain.dao.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;


public class Employee implements Serializable{

    private int id;
    private String email;
    private String firstname;
    private String secondname;
    private float salary;
    private Date dateOfBirth;
    private int departmantID;


    public Employee() {
    }
    public Employee(String email, String firstname, String secondname, float salary, Date dateOfBirth, int departmantID) {
        
        this.email = email;
        this.firstname = firstname;
        this.secondname = secondname;
        this.salary = salary;
        this.dateOfBirth = dateOfBirth;
        this.departmantID = departmantID;
    }
    

    public Employee(int id, String email, String firstname, String secondname, float salary, Date dateOfBirth, int departmantID) {
        this.id = id;
        this.email = email;
        this.firstname = firstname;
        this.secondname = secondname;
        this.salary = salary;
        this.dateOfBirth = dateOfBirth;
        this.departmantID = departmantID;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getDepartmantID() {
        return departmantID;
    }

    public void setDepartmantID(int departmantID) {
        this.departmantID = departmantID;
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + Objects.hashCode(this.email);
        hash = 97 * hash + Objects.hashCode(this.firstname);
        hash = 97 * hash + Objects.hashCode(this.secondname);
        hash = 97 * hash + Float.floatToIntBits(this.salary);
        hash = 97 * hash + Objects.hashCode(this.dateOfBirth);
        hash = 97 * hash + this.departmantID;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Float.floatToIntBits(this.salary) != Float.floatToIntBits(other.salary)) {
            return false;
        }
        if (this.departmantID != other.departmantID) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.firstname, other.firstname)) {
            return false;
        }
        if (!Objects.equals(this.secondname, other.secondname)) {
            return false;
        }
        if (!Objects.equals(this.dateOfBirth, other.dateOfBirth)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Employees{" + "id=" + id + ", "
                + "email=" + email + ", firstname=" + firstname + 
                ", secondname=" + secondname + ", salary=" + salary + 
                ", dateOfBirth=" + dateOfBirth + ", departmant_id=" + departmantID + '}';
    }



    
    

}
