
package com.vitalii.kushchin.domain.dao.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Departmant implements Serializable{

    private int id;
    private String name;
    private String email;
    private List<Employee> employees;
    
    public Departmant(){
        
    }

    public Departmant(String name, String email ) {
        
        this.name = name;
        this.email = email;
        
    }
    

    public Departmant(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
       
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    @Override
    public String toString() {
        return "Departmant{" + "id=" + id + ", name=" + name + ", email=" + email + ", employees=" + employees + '}';
    }
    
    
   

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.name);
        hash = 41 * hash + Objects.hashCode(this.email);
        hash = 41 * hash + Objects.hashCode(this.employees);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Departmant other = (Departmant) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.employees, other.employees)) {
            return false;
        }
        return true;
    }

}
