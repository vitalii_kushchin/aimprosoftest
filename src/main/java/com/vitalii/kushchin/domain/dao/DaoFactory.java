
package com.vitalii.kushchin.domain.dao;
import com.vitalii.kushchin.domain.mysql.*;

public abstract class DaoFactory {
    
    public static final String MYSQL = "MySQL"; 
    
    public abstract DepartmantDao  getDepartmantDao();
    public abstract EmployeeDao  getEmployeeDao();
    
    public static DaoFactory getDaoFactory(String daoFactoryName){
        if (daoFactoryName.equals(MYSQL)){
            return new MySqlDaoFactory();
        }
        return null;
        
    }
    
    
    
}
