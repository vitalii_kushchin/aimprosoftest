/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vitalii.kushchin.domain.dao;
import  com.vitalii.kushchin.domain.dao.entity.Employee;
import java.util.List;
public interface EmployeeDao extends BaseDao<Employee, Integer, String>{
    List<Employee> getEmployeebyDepartmant(Integer idDepartmant);
}
