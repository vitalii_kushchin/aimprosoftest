
package com.vitalii.kushchin.domain.dao;
import java.util.List;

public interface BaseDao<T,I,S> {

    void creat(T object);
    T read(I id);
    void update(T object);
    void remove(I id);
    List<T> getAll();
    boolean isUnique(S object);
        
}
