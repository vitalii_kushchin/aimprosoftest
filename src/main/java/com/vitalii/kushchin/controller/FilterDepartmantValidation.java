package com.vitalii.kushchin.controller;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import com.vitalii.kushchin.controller.utils.Validator;
import javax.servlet.RequestDispatcher;
import com.vitalii.kushchin.domain.dao.entity.Departmant;

@WebFilter(filterName = "FilterDepartmantValidation", urlPatterns = {"/DepartmantList", "/UpdateDepartmant"})
public class FilterDepartmantValidation implements Filter {

    private static final boolean debug = true;

    private FilterConfig filterConfig = null;

    public FilterDepartmantValidation() {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (httpRequest.getMethod().equalsIgnoreCase("POST")) {
            String departmantEmail = ((HttpServletRequest) request).getParameter("departmantEmail");
            String departmantName = ((HttpServletRequest) request).getParameter("departmantName");
            Validator validation = new Validator();
            if (validation.isEmailValid(departmantEmail) && (!validation.isEmpty(departmantName))) {
                chain.doFilter(request, response);

            } else {
                if (((Departmant) (httpRequest.getSession().getAttribute("departmant"))) != null) {

                    httpRequest.getSession().setAttribute("invalid", "The department name or email has not been updated.");
                    httpRequest.getSession().setAttribute("departmantName", departmantName);
                    httpRequest.getSession().setAttribute("departmantEmail", departmantEmail);
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/updateDepartmant.jsp");
                    rd.forward(request, response);
                } else {
                    httpRequest.getSession().setAttribute("invalid", "The department was not added because its name or email address was entered incorrectly");
                    httpRequest.getSession().setAttribute("departmantName", departmantName);
                    httpRequest.getSession().setAttribute("departmantEmail", departmantEmail);
                    RequestDispatcher rd = request.getRequestDispatcher("addDepartmant.jsp");
                    rd.forward(request, response);
                }

            }

        } else {
//            if ((((Departmant) (httpRequest.getSession().getAttribute("departmant"))) != null)) {
//                httpRequest.getSession().setAttribute("invalid", "");
//                chain.doFilter(request, response);
//
//            } else {

                httpRequest.getSession().setAttribute("invalid", "");
                httpRequest.getSession().setAttribute("departmantName", "");
                httpRequest.getSession().setAttribute("departmantEmail", "");
                chain.doFilter(request, response);
//            }

        }

    }

    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

}
