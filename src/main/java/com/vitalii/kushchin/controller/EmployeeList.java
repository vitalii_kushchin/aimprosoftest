package com.vitalii.kushchin.controller;

import com.vitalii.kushchin.domain.dao.DaoFactory;
import com.vitalii.kushchin.domain.dao.EmployeeDao;
import com.vitalii.kushchin.domain.dao.entity.Employee;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class EmployeeList extends HttpServlet {

    private EmployeeDao employeeDao = DaoFactory.getDaoFactory("MySQL").getEmployeeDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int idDepartmant = Integer.parseInt(request.getParameter("idDepartmant"));
        List<Employee> employeeList = employeeDao.getEmployeebyDepartmant(idDepartmant);
        HttpSession session = request.getSession();
        session.setAttribute("employeesList", employeeList);
        session.setAttribute("idDepartmant", idDepartmant);
        request.getRequestDispatcher("/WEB-INF/view/employeeList.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String firstName = request.getParameter("firstName");
        String secondName = request.getParameter("secondName");
        String email = request.getParameter("email");
        Float salary = Float.valueOf(request.getParameter("salary"));
        Date date = Date.valueOf(request.getParameter("date"));
        int idDepartmant = Integer.parseInt(String.valueOf(session.getAttribute("idDepartmant")));
        if (employeeDao.isUnique(email)) {
            Employee employee = new Employee(email, firstName, secondName, salary, date, idDepartmant);
            employeeDao.creat(employee);
            response.sendRedirect("/AimprosoftTestTwo/EmployeeList?idDepartmant=" + idDepartmant);
        } else {
            response.sendRedirect("badRegistration.jsp");
        }

        

    }

}
