package com.vitalii.kushchin.controller;

import com.vitalii.kushchin.domain.dao.DaoFactory;
import com.vitalii.kushchin.domain.dao.DepartmantDao;
import com.vitalii.kushchin.domain.dao.entity.Departmant;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UpdateDepartmant extends HttpServlet {

    DepartmantDao departmantDao = DaoFactory.getDaoFactory("MySQL").getDepartmantDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer idDepartmant = Integer.parseInt(request.getParameter("idDepartmant"));
        Departmant departmant = departmantDao.read(idDepartmant);
        HttpSession session = request.getSession();
        session.setAttribute("departmant", departmant);
        request.getRequestDispatcher("/WEB-INF/view/updateDepartmant.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer idDepartmant = ((Departmant) (request.getSession().getAttribute("departmant"))).getId();
        Departmant departmant = ((Departmant)(request.getSession().getAttribute("departmant")));
        String departmantName = request.getParameter("departmantName");
        String departmantEmail = request.getParameter("departmantEmail");
        if (departmantDao.isUnique(departmantName)|| departmant.getName().equals(departmantName)) {
            departmant = new Departmant(idDepartmant, departmantName, departmantEmail);
            departmantDao.update(departmant);
            HttpSession session = request.getSession();
            session.setAttribute("departmant", null);
            response.sendRedirect("/AimprosoftTestTwo/DepartmantList");
        } else {
            request.getSession().setAttribute("invalid", "This name already exsist");
            request.getRequestDispatcher("/WEB-INF/view/updateDepartmant.jsp").forward(request, response);
        }

    }

}
