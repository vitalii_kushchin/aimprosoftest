package com.vitalii.kushchin.controller;

import com.vitalii.kushchin.domain.dao.DaoFactory;
import com.vitalii.kushchin.domain.dao.EmployeeDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.vitalii.kushchin.domain.dao.entity.Employee;
import javax.servlet.http.HttpSession;
import java.sql.Date;

public class UpdateEmployee extends HttpServlet {

    EmployeeDao employeedao = DaoFactory.getDaoFactory("MySQL").getEmployeeDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer id = Integer.parseInt(request.getParameter("employeeID"));
        Employee employee = employeedao.read(id);
        HttpSession session = request.getSession();
        session.setAttribute("employeeUpdate", employee);
        request.getRequestDispatcher("/WEB-INF/view/updateEmployee.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Employee employee = ((Employee) (request.getSession().getAttribute("employeeUpdate")));
        Integer idEmployee = employee.getId();
        Integer idDepartmant = employee.getDepartmantID();
        String oldEmail = employee.getEmail();
        String firstName = request.getParameter("firstName");
        String secondName = request.getParameter("secondName");
        String newEmail = request.getParameter("email");
        Float salary = Float.valueOf(request.getParameter("salary"));
        Date date = Date.valueOf(request.getParameter("dateOfBirth"));
        if (employeedao.isUnique(newEmail) || oldEmail.equals(newEmail)) {
            employee = new Employee(idEmployee, newEmail, firstName, secondName, salary, date, idDepartmant);
            employeedao.update(employee);
            HttpSession session = request.getSession();
            session.setAttribute("employeeUpdate", null);
            response.sendRedirect("EmployeeList?idDepartmant=" + idDepartmant);
        }else{
            request.getSession().setAttribute("invalid", "This email already exsist");
            request.getRequestDispatcher("/WEB-INF/view/updateEmployee.jsp").forward(request, response);
        }

    }

}
