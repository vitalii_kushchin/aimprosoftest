package com.vitalii.kushchin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import com.vitalii.kushchin.domain.dao.DaoFactory;
import com.vitalii.kushchin.domain.dao.DepartmantDao;
import com.vitalii.kushchin.domain.dao.entity.Departmant;
import javax.servlet.http.HttpSession;

public class DepartmantList extends HttpServlet {

    DepartmantDao departmantDao = DaoFactory.getDaoFactory("MySQL").getDepartmantDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Departmant> departmantList = departmantDao.getAll();
        HttpSession session = request.getSession();
        session.setAttribute("departmantList", departmantList);
        request.getRequestDispatcher("/WEB-INF/view/departmantList.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String departmantName = request.getParameter("departmantName");
        String departmantEmail = request.getParameter("departmantEmail");
        if (departmantDao.isUnique(departmantName)) {
            Departmant departmant = new Departmant(departmantName, departmantEmail);
            System.out.println(departmant);
            departmantDao.creat(departmant);
            response.sendRedirect("/AimprosoftTestTwo/DepartmantList");    
        }else{
            response.sendRedirect("badRegistration.jsp");
        }
            
        
    }

}
