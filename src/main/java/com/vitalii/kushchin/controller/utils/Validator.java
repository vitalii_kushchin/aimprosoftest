package com.vitalii.kushchin.controller.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private static final String EMAIL = "^[a-zA-Z0-9]{4,10}@[a-zA-Z0-9]{2,10}\\.[a-zA-Z0-9]{2,4}$";

    public boolean isEmpty(String data) {
        if (data == null) {
            return true;
        }
        data = data.trim();
        return data.isEmpty();

    }

    public boolean isEmailValid(String email) {

        Pattern pattern = Pattern.compile(EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.find();
    }


}
