
package com.vitalii.kushchin.controller;

import com.vitalii.kushchin.domain.dao.DaoFactory;
import com.vitalii.kushchin.domain.dao.EmployeeDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteEmployee extends HttpServlet {

    EmployeeDao employeeDao = DaoFactory.getDaoFactory("MySQL").getEmployeeDao();
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int idEmployee = Integer.parseInt(request.getParameter("employeeID"));
        employeeDao.remove(idEmployee);
        int idDepartmant = Integer.parseInt(request.getParameter("idDepartmant"));
        request.getRequestDispatcher("EmployeeList?idDepartmant="+idDepartmant).forward(request, response);
        
   
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
    }

  
  
}
