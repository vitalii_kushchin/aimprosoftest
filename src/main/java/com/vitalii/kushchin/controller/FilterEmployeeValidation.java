package com.vitalii.kushchin.controller;

import com.vitalii.kushchin.controller.utils.Validator;
import com.vitalii.kushchin.domain.dao.entity.Employee;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(filterName = "FilterEmployeeValidation", urlPatterns = {"/EmployeeList", "/UpdateEmployee"})
public class FilterEmployeeValidation implements Filter {

    FilterConfig filterconfig = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterconfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (httpRequest.getMethod().equalsIgnoreCase("POST")) {
            String employeeEmail = httpRequest.getParameter("email");
            String firstName = httpRequest.getParameter("firstName");
            String secondName = httpRequest.getParameter("secondName");
            Validator validation = new Validator();
            if (validation.isEmailValid(employeeEmail) && (!validation.isEmpty(firstName)) && (!validation.isEmpty(secondName))) {

                chain.doFilter(request, response);

            } else {

                if (((Employee) (httpRequest.getSession().getAttribute("employeeUpdate"))) != null) {

                    httpRequest.getSession().setAttribute("invalid", "Sorry, but the employee data has not been updated");
                    httpRequest.getSession().setAttribute("firstName", firstName);
                    httpRequest.getSession().setAttribute("secondName", secondName);
                    httpRequest.getSession().setAttribute("email", employeeEmail);
                    RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/view/updateEmployee.jsp");
                    rd.forward(request, response);

                } else {
                    httpRequest.getSession().setAttribute("invalid", "Sorry, but the employee was not added");
                    httpRequest.getSession().setAttribute("firstName", firstName);
                    httpRequest.getSession().setAttribute("secondName", secondName);
                    httpRequest.getSession().setAttribute("email", employeeEmail);
                    RequestDispatcher rd = request.getRequestDispatcher("addEmployee.jsp");
                    rd.forward(request, response);
                }

            }
        } else {
            httpRequest.getSession().setAttribute("invalid", "");
            httpRequest.getSession().setAttribute("firstName", "");
            httpRequest.getSession().setAttribute("secondName", "");
            httpRequest.getSession().setAttribute("email", "");
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }

}
