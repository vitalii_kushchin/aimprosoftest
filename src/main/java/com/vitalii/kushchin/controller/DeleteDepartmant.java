package com.vitalii.kushchin.controller;

import com.vitalii.kushchin.domain.dao.DaoFactory;
import com.vitalii.kushchin.domain.dao.DepartmantDao;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteDepartmant extends HttpServlet {

    DepartmantDao departmantDao = DaoFactory.getDaoFactory("MySQL").getDepartmantDao();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int departmantId = Integer.parseInt(request.getParameter("idDepartmant"));
        departmantDao.remove(departmantId);
        request.getRequestDispatcher("/DepartmantList").forward(request, response);
        
        
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
